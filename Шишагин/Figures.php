<?php
abstract class BaseFigure
{
    protected const DEFAULT_MIN_VALUE = 1;
    protected const DEFAULT_MAX_VALUE = 10;
    protected $type='';

    abstract public function randomInit();
    abstract public function getArea();

    static public function compare($figure1, $figure2)
    {
        $area1 = $figure1->getArea();
        $area2 = $figure2->getArea();
        if ($area1 == $area2) {
            return 0;
        }
        return ($area1 > $area2) ? +1: -1;
    }
}

class Rectangle extends BaseFigure
{
    protected $length;
    protected $width;

    public function randomInit($min = BaseFigure::DEFAULT_MIN_VALUE,
                               $max = BaseFigure::DEFAULT_MAX_VALUE)
    {
        $this->length = rand($min, $max);
        $this->width = rand($min, $max);
    }

    public function getArea()
    {
        return $this->length * $this->width;
    }
}

class Circle extends BaseFigure
{
    protected $radius;

    public function randomInit($min = BaseFigure::DEFAULT_MIN_VALUE,
                               $max = BaseFigure::DEFAULT_MAX_VALUE)
    {
        $this->radius = rand($min, $max);
    }

    public function getArea()
    {
        return pow($this->radius, 2) * M_PI;
    }
}

class Triangle extends BaseFigure
{
    protected $side1;
    protected $side2;
    protected $side3;

    public function randomInit($min = BaseFigure::DEFAULT_MIN_VALUE,
                               $max = BaseFigure::DEFAULT_MAX_VALUE)
    {
        $this->side1 = rand($min, $max);
        $this->side2 = rand($min, $max);
        $this->side3 = rand($min, $max);
    }

    public function getArea()
    {
        $semiPerimeter = ($this->side1 + $this->side2 + $this->side3);
        return sqrt($semiPerimeter * ($semiPerimeter - $this->side1)
                                   * ($semiPerimeter - $this->side2)
                                   * ($semiPerimeter - $this->side3) );
    }
}

class FiguresCollection
{
    protected $items = array();

    public function populate($numberOfInstances, $min = 1, $max = 10)
    {
        $types = ["Rectangle", "Circle", "Triangle"];
        $numberOfTypes = count($types);

        for ($index = 0; $index < $numberOfInstances; $index++) {
            $typeIndex = rand(0, $numberOfTypes - 1);
            $classname = $types[$typeIndex];
            $instance  = new $classname($min, $max);
            $instance->randomInit();
            $this->items[] = $instance;
        }
    }

    public function sort($isAscending = true)
    {
        usort($this->items, array("BaseFigure", "compare"));
        if ($isAscending == false) {
            $this->items=array_reverse($this->items);
        }
    }

    public function writeToFile($fileName)
    {
        $encoded = serialize( $this->items );
        file_put_contents($fileName, $encoded);
    }

    public function readFromFile($fileName)
    {
        $data = file_get_contents($fileName);
        $decoded = unserialize($data);
        if (!empty($decoded)){
            $this->items = $decoded;
        }
    }

    public function printItemsWithArea()
    {
        echo "Printing array collection of size " . count($this->items) . "<BR/>";
        foreach($this->items as $item) {
            echo "* Item of type " . get_class($item) . " with area " . (string)$item->getArea() . "<BR/>";
        }
    }

    public function saveToFile($json_file)
    {
        echo "Writting collection to a file";
        $encoded = json_encode($this->items, true);
        echo $encoded;
        var_dump($encoded);
    }
}

echo "Starting";

$collection = new FiguresCollection();
$collection->populate(10);

$fileName="collection.data";
$collection->writeToFile($fileName);
$collection->readFromFile($fileName);

$collection->sort(false);
$collection->printItemsWithArea();
?>
