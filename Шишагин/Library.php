"""Выполнено в SQLite"""

--CREATE TABLE "авторы" (
-- "имя" char(200),
-- "книга" char(200),
-- "количество книг" INT(100)
--);

--INSERT INTO "авторы" VALUES ("Пикуль", "Пером и шпагой", 6);
--INSERT INTO "авторы" VALUES ("Пикуль", "Честь имею", NULL);
--INSERT INTO "авторы" VALUES ("Пикуль", "Океанский патруль", NULL);
--INSERT INTO "авторы" VALUES ("Пикуль", "Баязет", NULL);
--INSERT INTO "авторы" VALUES ("Пикуль", "Из тупика", NULL);
--INSERT INTO "авторы" VALUES ("Пикуль", "Плевелы", NULL);
--INSERT INTO "авторы" VALUES ("Ильф", "12 стульев", 2);
--INSERT INTO "авторы" VALUES ("Ильф", "Золотой теленок", NULL);
--INSERT INTO "авторы" VALUES ("Петров", "12 стульев", 2);
--INSERT INTO "авторы" VALUES ("Петров", "Золотой теленок", NULL);

SELECT "имя" FROM "авторы" WHERE "количество книг" < 6;
